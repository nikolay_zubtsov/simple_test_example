import allure
import pytest
import time
from webdriver_manager.chrome import ChromeDriverManager
#from allure_commons.types import AttachmentType
from selenium import webdriver
from selenium.webdriver.common.by import By



#@allure.severity(allure.severity_level.NORMAL)
class Test1():
    def setup_method(self, method):
        self.driver = webdriver.Chrome((ChromeDriverManager().install()))
        self.vars = {}

    def teardown_method(self, method):
        self.driver.quit()

    #@allure.severity(allure.severity_level.NORMAL)
    def test_search(self):
        self.driver.get("https://yandex.ru/")
        self.driver.set_window_size(1346, 706)
        self.driver.find_element(By.XPATH, "//span/input").send_keys("yandex")
        self.driver.find_element(By.XPATH, "//button[contains(.,\'Найти\')]").click()
        #time.sleep(10)
        #allure.attach(self.driver.get_screenshot_as_png(), name="Searchpage", attachment_type=AttachmentType.PNG)
        self.driver.close()
